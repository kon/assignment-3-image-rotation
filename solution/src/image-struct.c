#include "image-struct.h"
#include <stdlib.h>
struct image* init_empty(void) {
  return (struct image*)calloc(1, sizeof(struct image));
}
void free_image(struct image *img) {
  for (size_t i = 0; i < (img->height > img->width ? img->height : img->width); i++)
    free(img->data[i]);
  free(img->data);
  free(img);
}
