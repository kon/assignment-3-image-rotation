#include "transform.h"
#include "image-struct.h"


#include <stdint.h>
#include <stdio.h>

void swap_pixel(struct pixel *a, struct pixel *b) {

  struct pixel tmp = *a;
  *a = *b;
  *b = tmp;
}
void swap_size(size_t *a, size_t *b) {
  if (a == b)
    return;
  size_t tmp = *a;
  *a = *b;
  *b = tmp;
}
void top_to_bottom(struct image *img) {
  for (size_t j = 0; j < img->width; j++) {
    for (size_t i = 0; i < img->height / 2; i++) {
      swap_pixel(&img->data[i][j], &img->data[img->height - i - 1][j]);
    }
  }
}
void left_to_right(struct image *img) {
  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width / 2; j++) {
      swap_pixel(&img->data[i][j], &img->data[i][img->width - j - 1]);
    }
  }
}
void transpose(struct image *img) {

  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width; j++) {
      if (j <= i && i < img->width)
        continue;
      swap_pixel(&img->data[i][j], &img->data[j][i]);
    }
  }
  swap_size(&img->height, &img->width);
}
void rotate(struct image *img, int32_t angle) {
  angle += 360;
  angle = angle % 360;
  switch (angle)
  {
  case 0:
    return;
    break;
  case 90:
    transpose(img);
    left_to_right(img);
    break;
  case 180:
    top_to_bottom(img);
    left_to_right(img);
    break;
  case 270:
    transpose(img);
    top_to_bottom(img);
    break;
  }
}
