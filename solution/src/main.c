#include "bmp.h"
#include "file_utils.h"
#include "image-struct.h"
#include "transform.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define print_error(e) fprintf(stderr, "%s", e)

int main(int argc, char **argv) {
  if (argc != 4) {
    print_error("must be 3 params");
    return 0;
  }
  FILE *input_file;
  if (open_bmp_file_reading(argv[1], &input_file) != FILE_OK) {
    print_error("error when opennig input file");
    return 0;
  }
  struct image *in_img = init_empty();
  struct bmp_header in_header;
  read_status status_r = from_bmp(input_file, in_img, &in_header);
  if (status_r != READ_OK) {
    if (status_r == READ_HEADER_ERROR)
      print_error("error while reading header");
    else
      print_error("error while reading pixels");
    free_image(in_img);
    return 0;
  }

  int32_t angle = atoi(argv[3]);

  rotate(in_img, angle);
  FILE *output_file;
  if (open_bmp_file_writing(argv[2], &output_file) != FILE_OK) {
    print_error("error when opennig output file");
    free_image(in_img);
    return 0;
  }

  modify_header(&in_header, in_img);
  write_status status_w = to_bmp(output_file, in_img, &in_header);
  if (status_w != WRITE_OK) {
    if (status_w == WRITE_HEADER_ERROR)
      print_error( "error while writing header");
    else if (status_w == WRITE_PADDING_ERROR)
      print_error("error while writing padding");
    else
      print_error("error while writing pixels");
    free_image(in_img);
    return 0;
  }

  free_image(in_img);
  return 0;
}
