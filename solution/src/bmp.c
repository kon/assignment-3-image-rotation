#include "bmp.h"
#include "image-struct.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


int32_t get_padding(struct image *img) {
  return (4 - ((int32_t)img->width * (int32_t)sizeof(struct pixel)) % 4) % 4;
}
read_status read_header(FILE *in, struct bmp_header *header) {
  int32_t cnt = (int32_t) fread(header, sizeof(*header), 1, in);

  if (cnt != 1) {
    return READ_HEADER_ERROR;
  }
  return READ_OK;
}
read_status read_pixels(FILE *in, struct image *img) {
  int32_t padding = get_padding(img);
  size_t arr_dim = img->height > img->width ? img->height : img->width;

  img->data = calloc(arr_dim, sizeof(void *));
  if (img->data == NULL){
    return ALLOCATION_ERROR;
  }
  for (size_t i = 0; i < arr_dim; i++){
    img->data[i] = calloc(arr_dim, sizeof(struct pixel));
    if (img->data[i] == NULL)
      return ALLOCATION_ERROR;
  }
  for (int32_t i = (int32_t)img->height - 1; i >= 0; i--) {
    int32_t res =
        (int32_t)fread(img->data[i], sizeof(struct pixel), img->width, in); // read row
    if (res != (int32_t)img->width)
      return READ_PIXELS_ERROR;

    res = fseek(in, padding, SEEK_CUR); // padding
    if (res != 0)
      return READ_PIXELS_ERROR;
  }
  return READ_OK;
}
read_status from_bmp(FILE *in, struct image *img, struct bmp_header *header) {
  read_status status = read_header(in, header);
  img->width = header->biWidth;
  img->height = header->biHeight;
  if (status != READ_OK)
    return status;
  return read_pixels(in, img);
}
void modify_header(struct bmp_header *header, struct image *img) {
  header->biWidth = img->width;
  header->biHeight = img->height;
  header->bfileSize =
      sizeof(struct bmp_header) +
      (img->width * sizeof(struct pixel) + get_padding(img)) * (img->height);
  header->biSizeImage =
      (img->width * sizeof(struct pixel) + get_padding(img)) * (img->height);
}

write_status to_bmp(FILE *out, struct image *img, struct bmp_header *header) {
  char padding_buf[4] = {0};
  int32_t padding = get_padding(img);
  int32_t res = (int32_t)fwrite(header, sizeof(struct bmp_header), 1, out);
  if (res != 1)
    return WRITE_HEADER_ERROR;
  for (int32_t i = (int32_t)img->height - 1; i >= 0; i--) {
    res = (int32_t)fwrite(img->data[i], sizeof(struct pixel), img->width, out);
    if (res != (int32_t)img->width)
      return WRITE_PIXELS_ERROR;
    res = (int32_t)fwrite(padding_buf, 1, padding, out); // padding
    if (res != padding)
      return WRITE_PADDING_ERROR;
  }
  return WRITE_OK;
}
