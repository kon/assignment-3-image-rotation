#include "file_utils.h"
#include <stdio.h>

file_status open_file(char *path, FILE **result, char *mode) {
  *result = fopen(path, mode);
  if (*result == NULL)
    return FILE_OPEN_ERROR;
  return FILE_OK;
}
file_status open_bmp_file_reading(char *path, FILE **result) {
  return open_file(path, result, "rb");
}
file_status open_bmp_file_writing(char *path, FILE **result) {
  return open_file(path, result, "wb");
}
file_status close_file(FILE *f) {
  if (fclose(f) == 0)
    return FILE_OK;
  return FILE_CLOSE_ERROR;
}
