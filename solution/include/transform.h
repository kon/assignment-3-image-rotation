#include "image-struct.h"
#include <stdint.h>

#ifndef TRANSFORM
#define TRANSFORM
void rotate(struct image *img, int32_t angle);
#endif
