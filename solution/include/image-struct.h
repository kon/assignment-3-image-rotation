#include <stddef.h>
#include <stdint.h>

#ifndef IMAGE_STRUCT
#define IMAGE_STRUCT
struct pixel {
  uint8_t b, g, r;
};
struct image {
  size_t width, height;
  struct pixel **data; // array of pointers to each row
};
struct image *init_empty(void);
void free_image(struct image *img);
#endif
