#include "image-struct.h"
#include <stdint.h>
#include <stdio.h>

#ifndef BMP
#define BMP
#pragma pack(push, 1)
struct bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};
#pragma pack(pop)

typedef enum {
  READ_OK = 0,
  READ_HEADER_ERROR,
  READ_PIXELS_ERROR,
  ALLOCATION_ERROR
} read_status;
typedef enum {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
  WRITE_PIXELS_ERROR,
  WRITE_PADDING_ERROR
} write_status;
read_status from_bmp(FILE *in, struct image *img, struct bmp_header *header);
write_status to_bmp(FILE *out, struct image *img, struct bmp_header *header);
void modify_header(struct bmp_header *header, struct image *img);
#endif
