#include <stdio.h>
#ifndef FILE_UTILS
#define FILE_UTILS
typedef enum { FILE_OK = 0, FILE_OPEN_ERROR, FILE_CLOSE_ERROR } file_status;
file_status open_bmp_file_reading(char *path, FILE **result);
file_status open_bmp_file_writing(char *path, FILE **result);
file_status close_file(FILE *f);
#endif
